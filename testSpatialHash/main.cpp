//
//  main.cpp
//  testSpatialHash
//
//  Created by Damian Stewart on 20/11/15.
//
//

#include <iostream>
#include "SpatialHash.h"
using glm::vec3;

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

const SpatialHash::Params params = { .cellSize = 0.01f, .size = 20000 };

TEST_CASE( "objects can be inserted and retrieved by location", "[SpatialHash]" ) {
    
    SpatialHash hash(params);
	
	const SpatialHash::ElementID objectId = 0;
	SpatialHash::AABB aabb(vec3(0, 0, 0), vec3(0.9, 0.9, 0.9));
	
	REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.min).size() == 0 );
	
	hash.AddElement(objectId, aabb);
	
	REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.min).size() >= 1 );
		REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.min)[0] == objectId );
	REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates((aabb.min+aabb.max)/2.0f).size() >= 1 );
	REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates((aabb.min+aabb.max)/2.0f)[0] == objectId );
	REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.max).size() >= 1 );
	REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.max)[0] == objectId );
	
}

TEST_CASE( "no false positives", "[SpatialHash]" ) {
	
	SpatialHash hash(params);
	
	const SpatialHash::ElementID objectId = 0;
	SpatialHash::AABB aabb(vec3(0, 0, 0), vec3(params.cellSize*0.99, params.cellSize*0.99, params.cellSize*0.99));
    hash.AddElement(objectId, aabb);
    
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.min).size() >= 1 );
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.max).size() >= 1 );

	REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.max+vec3(0.1,0.1,0.1)).size() == 0 );

}

TEST_CASE( "cells are cleared after calling clear()", "[SpatialHash]" ) {
    
    SpatialHash hash(params);
    
    SpatialHash::AABB aabb(vec3(0, 0, 0), vec3(0.9, 0.9, 0.9));
    const SpatialHash::ElementID objectId = 0;
    
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.min).size() == 0 );
    
    hash.AddElement(objectId, aabb);
    
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.min).size() >= 1 );
    
    hash.Clear();
    
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.min).size() == 0 );
    
    hash.AddElement(objectId, aabb);
    
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb.min).size() >= 1 );
    
}

TEST_CASE ("multiple non-overlapping objects can be inserted and retrieved", "[SpatialHash]") {
    
    SpatialHash hash(params);
    
    SpatialHash::AABB aabb1(vec3(0, 0, 0), vec3(0.3, 0.3, 0.3));
    const SpatialHash::ElementID objectId1 = 0;
	SpatialHash::AABB aabb2(vec3(0.5, 0, 0), vec3(0.8, 0.3, 0.3));
    const SpatialHash::ElementID objectId2 = 1;
    
    hash.AddElement(objectId1, aabb1);
    hash.AddElement(objectId2, aabb2);
    
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb1.min).size() >= 1 );
	
	auto overlapping1 = hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb1.min);
REQUIRE( std::find(overlapping1.begin(), overlapping1.end(), objectId1) != overlapping1.end() );
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb2.min).size() >= 1 );
	
	auto overlapping2 = hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb2.min);
	REQUIRE( std::find(overlapping2.begin(), overlapping2.end(), objectId2) != overlapping2.end() );
}

TEST_CASE ("multiple overlapping objects can be inserted and retrieved", "[SpatialHash]") {
    
    SpatialHash hash(params);
	
	SpatialHash::AABB aabb1(vec3(0, 0, 0), vec3(0.02, 0.02, 0.02));
	const SpatialHash::ElementID objectId1 = 0;
	SpatialHash::AABB aabb2(vec3(0.01, 0, 0), vec3(0.03, 0.02, 0.02));
	const SpatialHash::ElementID objectId2 = 1;
	
    hash.AddElement(objectId1, aabb1);
    hash.AddElement(objectId2, aabb2);
    
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb1.min).size() >= 1 );
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb1.min)[0] == objectId1 );
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb2.max).size() >= 1 );
    REQUIRE( hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb2.max)[0] == objectId2 );
    
    auto overlapping = hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(vec3(0.019, 0.01, 0.01));
    REQUIRE( overlapping.size() >= 2 );
    REQUIRE( std::find(overlapping.begin(), overlapping.end(), objectId1) != overlapping.end() );
    REQUIRE( std::find(overlapping.begin(), overlapping.end(), objectId2) != overlapping.end() );
    
    overlapping = hash.GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(vec3(0.025, 0.015, 0.015));
    REQUIRE( overlapping.size() >= 2 );
    REQUIRE( std::find(overlapping.begin(), overlapping.end(), objectId1) != overlapping.end() );
    REQUIRE( std::find(overlapping.begin(), overlapping.end(), objectId2) != overlapping.end() );
}
