/*
 *  SpatialHash.h
 *  SpatialHash
 *
 *  Created by Damian Stewart on 20/11/15.
 *
 *
 */

#ifndef SpatialHash_
#define SpatialHash_

#include <vector>
#include <set>
#include <glm/glm.hpp>

using std::vector;
using glm::vec3;
using std::set;

/** 3d non-hierarchical spatial hashing implementation
 
 based on Teschner, Heidelberger, Müller, Pomeranets, Gross "Optimized Spatial Hashing for Collision Detection of Deformable Objects" (VMV 2003)
 
 */

class SpatialHash
{
public:
    
    typedef uint32_t ElementID;
    
    struct AABB {
        vec3 min;
        vec3 max;

		AABB(vec3 _min, vec3 _max) : min(_min), max(_max) {}
		
        AABB(vec3 center, float radius) {
            assert(radius >= 0);
            vec3 offset(radius,radius,radius);
            min = center - offset;
            max = center + offset;
        }
        
        void AddVertex(const vec3& vertex)
        {
            min.x = std::min(min.x, vertex.x);
            min.y = std::min(min.y, vertex.y);
            min.z = std::min(min.z, vertex.z);
            max.x = std::max(max.x, vertex.x);
            max.y = std::max(max.y, vertex.y);
            max.z = std::max(max.z, vertex.z);
        }
    };
    
    struct Params {
        float cellSize; // edge length of the cubic volume that defines a single hash buckets/cells cubes). for tetrahedral meshes, should be ~ average tetra edge length
        unsigned int size; // number of entries in the hash map. for tetrahedral meshes, try ~8x the number of tetrahedral elements
    };
    
    
    SpatialHash(const struct Params params);
	
    void AddElement(ElementID elementId, struct AABB aabb);
    
    void Clear() { ++mTimestamp; }
    
    const vector<ElementID>& GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(vec3 position);
    vector<ElementID> GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(const AABB &aabb);
    void GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(const SpatialHash::AABB &aabb, vector<SpatialHash::ElementID> &result);

private:
    static unsigned int GetHash(int x, int y, int z)
    {
        const int p1 = 73856093;
        const int p2 = 19349663;
        const int p3 = 83492791;
        return unsigned(x*p1) ^ unsigned(y*p2) ^ unsigned(z*p3);
    }
    
    struct CellDefinition
    {
        int x;
        int y;
        int z;
        
        unsigned int GetHash() const { return SpatialHash::GetHash(x, y, z); }
    };
    
    CellDefinition GetCell(vec3 position);
    
    struct TimestampedElementList
    {
        unsigned int timestamp;
        vector<ElementID> elements;
    };
    
    TimestampedElementList& GetElementList(const CellDefinition& cell);
    vector<TimestampedElementList> mEntries;
    
    float mCellSize = 0.01f;
    
    unsigned int mTimestamp = 0;
    
};

#endif
