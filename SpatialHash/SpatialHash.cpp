
#include "SpatialHash.h"

SpatialHash::SpatialHash(const struct Params params)
: mEntries(params.size), mCellSize(params.cellSize)
{
}

void SpatialHash::AddElement(ElementID elementId, struct AABB aabb)
{
    auto minCell = GetCell(aabb.min);
    auto maxCell = GetCell(aabb.max);
    auto cell = minCell;
    for (cell.x=minCell.x; cell.x<=maxCell.x; ++cell.x) {
        for (cell.y=minCell.y; cell.y<=maxCell.y; ++cell.y) {
            for (cell.z=minCell.z; cell.z<=maxCell.z; ++cell.z) {
                auto& elementList = GetElementList(cell);
                if (elementList.timestamp != mTimestamp) {
                    elementList.elements.clear();
                    elementList.timestamp = mTimestamp;
                }
                
                elementList.elements.push_back(elementId);
            }
        }
    }
    
}

SpatialHash::CellDefinition SpatialHash::GetCell(vec3 position)
{
    CellDefinition cell;
    cell.x = int(position.x / mCellSize);
    cell.y = int(position.y / mCellSize);
    cell.z = int(position.z / mCellSize);
    return cell;
}

SpatialHash::TimestampedElementList& SpatialHash::GetElementList(const CellDefinition& cell)
{
    auto hash = cell.GetHash();
    return mEntries[hash % mEntries.size()];
}

const vector<SpatialHash::ElementID>& SpatialHash::GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(vec3 position)
{
    const auto& elementList = GetElementList(GetCell(position));
    if (elementList.timestamp == mTimestamp) {
        return elementList.elements;
    }
    static const vector<ElementID> emptyList;
    return emptyList;
}

vector<SpatialHash::ElementID> SpatialHash::GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(
        const SpatialHash::AABB &aabb)
{
    vector<SpatialHash::ElementID> result;
    GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(aabb, result);
    return result;
}

void
SpatialHash::GetPotentiallyOverlappingObjectsUnorderedWithDuplicates(const SpatialHash::AABB &aabb, vector<SpatialHash::ElementID> &result)
{
    result.clear();
    auto minCell = GetCell(aabb.min);
    auto maxCell = GetCell(aabb.max);
    auto cell = minCell;
    for (cell.x=minCell.x; cell.x<=maxCell.x; ++cell.x) {
        for (cell.y = minCell.y; cell.y <= maxCell.y; ++cell.y) {
            for (cell.z = minCell.z; cell.z <= maxCell.z; ++cell.z) {
                const auto &list = GetElementList(cell);
                result.insert(result.end(), list.elements.begin(), list.elements.end());
            }
        }
    }
}

